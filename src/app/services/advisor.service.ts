import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Advisor } from '../models/Advisor';

@Injectable()
export class AdvisorService {
  advisorsCollection: AngularFirestoreCollection<Advisor>;
  advisors: Observable<Advisor[]>;
  advisorDoc: AngularFirestoreDocument<Advisor>;

  constructor(public afs: AngularFirestore) { 
    this.advisorsCollection = this.afs.collection('advisors', ref => ref.orderBy('name','asc'));

    this.advisors = this.advisorsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Advisor;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getAdvisors(){
    return this.advisors;
  }

  addAdvisor(Advisor: Advisor){
    this.advisorsCollection.add(Advisor);
  }

  deleteAdvisor(Advisor: Advisor){
    this.advisorDoc = this.afs.doc(`advisors/${Advisor.id}`);
    this.advisorDoc.delete();
  }

  updateAdvisor(Advisor: Advisor){
    this.advisorDoc = this.afs.doc(`advisors/${Advisor.id}`);
    this.advisorDoc.update(Advisor);
  }

}