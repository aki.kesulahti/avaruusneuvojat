import { Component, OnInit } from '@angular/core';
import { AdvisorService } from '../../services/advisor.service';
import { Advisor } from '../../models/Advisor';


@Component({
  selector: 'app-advisors',
  templateUrl: './advisors.component.html',
  styleUrls: ['./advisors.component.css']
})
export class AdvisorsComponent implements OnInit {
  panelOpenState: boolean = false;
  advisors: Advisor[];
  advisor: Advisor = {
    name: '',
    address: '',
    organization: ''
  }

  constructor(private advisorService: AdvisorService) { }

  ngOnInit() {
    this.advisorService.getAdvisors().subscribe(advisors => {
      this.advisors = advisors;
    });
  }

  deleteAdvisor(event, advisor) {
    this.advisorService.deleteAdvisor(advisor);
  }

  editAdvisor(event, advisor) {
    this.advisorService.updateAdvisor(advisor);
  }

}
