import { Component, OnInit } from '@angular/core';
import { AdvisorService } from '../../services/advisor.service';
import { Advisor } from '../../models/Advisor';

@Component({
  selector: 'app-add-advisor',
  templateUrl: './add-advisor.component.html',
  styleUrls: ['./add-advisor.component.css']
})
export class AddAdvisorComponent implements OnInit {
  advisor: Advisor = {
    name: '',
    address: '',
    organization: ''
  }

  constructor(private advisorService: AdvisorService) { }

  ngOnInit() {
  }

  addAdvisor() {
    if(this.advisor.name != '' && this.advisor.organization != '' && this.advisor.address != ''){
      this.advisorService.addAdvisor(this.advisor);
      this.advisor.name = '';
      this.advisor.organization = '';
      this.advisor.address = '';
    }
  }

}
