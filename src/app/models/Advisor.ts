export interface Advisor {
    id?:string;
    name?:string;
    organization?:string;
    address?:string;
}